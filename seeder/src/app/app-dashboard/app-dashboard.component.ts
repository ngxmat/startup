import { Component } from '@angular/core';
import { map } from 'rxjs/operators';
import { Breakpoints, BreakpointState, BreakpointObserver } from '@angular/cdk/layout';

@Component({
  selector: 'app-app-dashboard',
  templateUrl: './app-dashboard.component.html',
  styleUrls: ['./app-dashboard.component.css']
})
export class AppDashboardComponent {
  features: any[] =[
    {
      title: "Project Info",
      content: `
      ## Do It All using Angular 6.x and Material 2.x for Frontend Developers

      Here you will learn the most important features and core functionalities of Angular 6.x along with Material 2.x.
      
      You will be able to learn how to create a real-world app from zero to hero.
      
      Learn how to optimize app perfomance.
      `
    },
    {
      title: "Core Features",
      content: `
        ## Road map

        List of tasks that will be done on this repo in a near future ...

        ~~Setting up project~~
        
        ~~Bootstraping~~

        ~~Basic form and validation example~~

        Basic layout demo for **grid and _typography_**.

        Extended Forms and Validations

        Advanced Forms includes CRUD, sorting, paging, and searching or filtering records

        Master/Details Forms 

        Learn RxJS to contro the overall forms events

        Application Based Messaging

        Authentication with JWT Tokens

        Bonus features: Know how to create a seamless CMS based web application

      `
    },
    {
      title: "Entry-level",
      content: `
      ## You will learn how to's on the ff...
      - Bootstraping
      - Setup default theming
      - Navigation
      - Sidebar
      - App Routing, Lazy loading
      - Basic Forms 
      - Extended Forms with Validations
      - etc.

    `
    },
    {
      title: "Intermediate to Advanced Skills",
      content: `
      ## You will learn how to's on the ff...
      - CRUD using MockServices
      - Datatables (Sort, Paging, and Search)
      - Learn how to create sample form with master-details
      - Learn how to use Rxjs 6.x the smart way
      - Learn how to enabled Security and Route Guard using JWT Tokens
      - learn how to create a Seamless CMS based web application
      - etc.

    `
    }
  ]


  /** Based on the screen size, switch from standard to one column per row */
  cards = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
    map(({ matches }) => {
      if (matches) {
        return [
          { cols: 1, rows: 1 },
          { cols: 1, rows: 1 },
          { cols: 1, rows: 1 },
          { cols: 1, rows: 1 }
        ];
      }

      return [
        { cols: 2, rows: 1 },
        { cols: 1, rows: 2 },
        { cols: 1, rows: 1 },
        { cols: 1, rows: 1 }
      ];
    })
  );

  constructor(private breakpointObserver: BreakpointObserver) {}
}
