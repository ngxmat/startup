import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DemoComponent } from './demo/demo.component';
import { AppDashboardComponent } from './app-dashboard/app-dashboard.component';
import { FormExampleAppModule } from './forms/form-example/form-example.module';


const routes: Routes = [
  {
      path: '', redirectTo: 'dashboard', pathMatch: 'full',
  },
  {
      path: '',
      children: [
          {
              path: 'dashboard',
              component: AppDashboardComponent,
          },
          { 
            path: 'forms',
            loadChildren: () => FormExampleAppModule
          },    

          
      ]
  }


  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
