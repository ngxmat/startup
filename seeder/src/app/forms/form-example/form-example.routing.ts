import { Routes } from '@angular/router';
import { BasicFormComponent } from './containers/basic/basic.component';

export const BasicRoutes: Routes = [
    {
        path: '', redirectTo: 'basic', pathMatch: 'full',
    },
    {
        path: '',
        children: [{
            path: 'basic',
            component: BasicFormComponent
        }]
    }, 



    
];
