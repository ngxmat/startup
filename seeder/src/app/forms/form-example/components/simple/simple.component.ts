import {Component, Input, Output, EventEmitter} from '@angular/core';
import { FormGroup } from '@angular/forms';


@Component({
  selector: 'simple-form',
  styleUrls: ['simple.component.scss'],
  templateUrl: './simple.component.html'
})
export class SimpleFormComponent {
  @Input()
  parent: FormGroup;

  @Output()
  submit = new EventEmitter<any>();

  checked = true;
  disabled = false;




  onSubmit(event) {
    event.stopPropagation();

    this.submit.emit(this.parent);
  }
}
