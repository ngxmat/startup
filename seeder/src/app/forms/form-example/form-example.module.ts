import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MaterialModule } from 'src/app/material.module';

import { BasicFormComponent } from './containers/basic/basic.component';
import { SimpleFormComponent } from './components/simple/simple.component';
import { BasicRoutes } from './form-example.routing';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    RouterModule.forChild(BasicRoutes),
  ],
  declarations: [
    BasicFormComponent,
    SimpleFormComponent
  ],
  exports: [
    BasicFormComponent,
  ],
  schemas: [ NO_ERRORS_SCHEMA ]
})
export class FormExampleAppModule {}