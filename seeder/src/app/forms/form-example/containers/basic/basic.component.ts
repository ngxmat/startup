import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-basic-form',
  templateUrl: './basic.component.html',
  styleUrls: ['basic.component.scss']
})
export class BasicFormComponent implements OnInit {

  form = this.fb.group({
    name: ['', Validators.required],
    age: [21, Validators.required],
    comment: ['', Validators.required]
  });

  constructor(private fb: FormBuilder) {
  }

  async ngOnInit() {
   
  }

  async submitForm(request: FormGroup) {
    console.log(request.value);
  }

  get f() { return this.form.controls; }
}
