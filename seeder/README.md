# Seeder

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.7.

You must have at least the following versions of:

npm: 6.3.0
node: 9.8.0
tsc: 2.7.2
Angular-cli: 6.1.3

Package                           Version
-----------------------------------------------------------
@angular-devkit/architect         0.6.8
@angular-devkit/build-angular     0.6.8
@angular-devkit/build-optimizer   0.6.8
@angular-devkit/core              0.6.8
@angular-devkit/schematics        0.6.8
@angular/cdk                      6.4.5
@angular/cli                      6.0.8
@angular/material                 6.4.5
@ngtools/webpack                  6.0.8
@schematics/angular               0.6.8
@schematics/update                0.6.8
rxjs                              6.2.2
typescript                        2.7.2
webpack                           4.8.3


## Install Dependencies

- open your terminal and execute the ff commands w/out quotes,

`cd seeder`
`npm install`

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

or run, `ng serve --port 4200 -o` to automatically open.
