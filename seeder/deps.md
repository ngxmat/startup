
## Developement Dependencies

- npm i --save hammerjs
- ng add @angular/material
- ng add @angular/material@6.4.5

- ng generate @angular/material:material-nav --name="app-nav" --module=app.module
- ng generate @angular/material:material-dashboard --name="app-dashboard" --module=app.module


# Other deps
- https://www.npmjs.com/package/ngx-markdown

